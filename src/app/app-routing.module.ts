import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'about',
    loadChildren: () => import ('./pages/about/about.module').then((m) => m.AboutModule),
  },
  {
    path: 'form',
    loadChildren: () => import ('./pages/form/form.module').then((m) => m.FormModule),
  },
  {
    path: 'foto',
    loadChildren: () => import ('./pages/foto/foto.module').then((m) => m.FotoModule),
  },
  {
    path: 'graphic',
    loadChildren: () => import ('./pages/graphic/graphic.module').then((m) => m.GraphicModule),
  },
  {
    path: 'home',
    loadChildren: () => import ('./pages/home/home.module').then((m) => m.HomeModule),
  },
  {
    path: 'music',
    loadChildren: () => import ('./pages/music/music.module').then((m) => m.MusicModule),
  },
  {
    path: 'video',
    loadChildren: () => import ('./pages/video/video.module').then((m) => m.VideoModule),
  },
  {
    path: 'detail/:id',
    loadChildren: () => import ('./pages/detail/detail.module').then((m) => m.DetailModule),
  }
  /* {
    path: 'detail/name?=name',
    loadChildren: () => import ('./pages/detail/detail.module').then((m) => m.DetailModule),
  } */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
