import { Component, OnInit } from '@angular/core';

import { Ihome } from './../model/Ihome';
import { HomeService } from './../service/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public dataHome!: Ihome[];

  constructor(private homeService: HomeService) {
    //empty
   }

  ngOnInit(): void {
    this.gethomeData();
    console.log(this.dataHome);
  }

  public gethomeData(): void {
    this.homeService.getHomeData().subscribe(
      (data: Ihome[]) => {
        console.log(data);
        this.dataHome = data;
      }
    )
  }

}
