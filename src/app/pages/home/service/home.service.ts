import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';


import { ENDPOINTS } from '../../../../endpointUrls/endpoints';
import { Ihome } from './../model/Ihome';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  private homeUrl: string = ENDPOINTS.home;

  
  constructor(private http: HttpClient) { 
    //empty
  }

  public getHomeData(): Observable<Ihome[]> {
    return this.http.get(this.homeUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
