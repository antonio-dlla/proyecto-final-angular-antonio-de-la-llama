import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { DetailService } from './../service/detail.service';
import { Idetail } from './../model/Idetail';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  public detail!: Idetail;
  private detailId!: string | null;

  constructor(
    private detailService: DetailService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getDetail();
  }

  public getDetail(): void {
    this.route.paramMap.subscribe((params) => {
      this.detailId = params.get('id');
    });
    this.detailService
      .getDetail(Number(this.detailId))
      .subscribe((data: Idetail) => {
        this.detail = data;
      });
  }
}
