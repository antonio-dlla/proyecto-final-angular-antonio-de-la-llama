import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';


import { ENDPOINTS } from '../../../../endpointUrls/endpoints';
import { Idetail } from './../model/Idetail';

@Injectable({
  providedIn: 'root'
})
export class DetailService {
  private detailUrl: string = ENDPOINTS.fotoDetail;

  
  constructor(private http: HttpClient) { 
    //empty
  }

  public getDetail(id: number): Observable<Idetail> {
    return this.http.get(this.detailUrl + id).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
