import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';


import { ENDPOINTS } from '../../../../endpointUrls/endpoints';
import { Igraphic } from './../model/Igraphic';

@Injectable({
  providedIn: 'root'
})
export class GraphicService {

  private graphicUrl: string = ENDPOINTS.graphic;

  
  constructor(private http: HttpClient) { 
    //empty
  }

  public getGraphicData(): Observable<Igraphic[]> {
    return this.http.get(this.graphicUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
