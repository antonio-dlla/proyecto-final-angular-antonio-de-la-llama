import { Component, OnInit } from '@angular/core';

import { Igraphic } from './../model/Igraphic';
import { GraphicService } from './../service/graphic.service';

@Component({
  selector: 'app-graphic',
  templateUrl: './graphic.component.html',
  styleUrls: ['./graphic.component.scss']
})
export class GraphicComponent implements OnInit {

  public dataGraphic!: Igraphic[];

  constructor(private graphicService: GraphicService) {
    //empty
   }

  ngOnInit(): void {
    this.getgraphicData();
    console.log(this.dataGraphic);
  }

  public getgraphicData(): void {
    this.graphicService.getGraphicData().subscribe(
      (data: Igraphic[]) => {
        console.log(data);
        this.dataGraphic = data;
      }
    )
  }

}
