export interface Igraphic{
    id: number;
    name: string;
    src: string;
    description: string | null;
    
}