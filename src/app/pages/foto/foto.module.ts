import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FotoRoutingModule } from './foto-routing.module';
import { FotoComponent } from './foto-component/foto.component';
import { FotoDetailComponent } from './foto-component/foto-detail/foto-detail.component';


@NgModule({
  declarations: [FotoComponent, FotoDetailComponent],
  imports: [
    CommonModule,
    FotoRoutingModule
  ]
})
export class FotoModule { }
