export interface Ifoto {
    id: number;
    name: string;
    src: string;
    description: string | null;
    
}