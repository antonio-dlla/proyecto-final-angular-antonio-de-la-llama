import { Component, OnInit } from '@angular/core';

import { Ifoto } from './../model/Ifoto';
import { FotoService } from './../service/foto.service';

@Component({
  selector: 'app-foto',
  templateUrl: './foto.component.html',
  styleUrls: ['./foto.component.scss']
})
export class FotoComponent implements OnInit {

  public dataFoto!: Ifoto[];

  constructor(private fotoService: FotoService) {
    //empty
   }

  ngOnInit(): void {
    this.getfotoData();
  }

  public getfotoData(): void {
    this.fotoService.getFotoData().subscribe(
      (data: Ifoto[]) => {
        console.log(data);
        this.dataFoto = data;
      }
    )
  }
   
  

}
