import { Ifoto } from './../../model/Ifoto';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-foto-detail',
  templateUrl: './foto-detail.component.html',
  styleUrls: ['./foto-detail.component.scss']
})
export class FotoDetailComponent implements OnInit {
  @Input() public fotoDetail!: Ifoto;
  constructor() { }

  ngOnInit(): void {
  }

}
