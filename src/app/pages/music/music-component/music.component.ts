import { Component, OnInit } from '@angular/core';

import { Imusic } from '../model/Imusic';
import { MusicService } from '../service/music.service';

@Component({
  selector: 'app-music',
  templateUrl: './music.component.html',
  styleUrls: ['./music.component.scss']
})
export class MusicComponent implements OnInit {

  public dataMusic!: Imusic[];

  constructor(private musicService: MusicService) {
    //empty
   }

  ngOnInit(): void {
    this.getmusicData();
    console.log(this.dataMusic);
  }

  public getmusicData(): void {
    this.musicService.getMusicData().subscribe(
      (data: Imusic[]) => {
        console.log(data);
        this.dataMusic = data;
      }
    )
  }

}
