import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';


import { ENDPOINTS } from '../../../../endpointUrls/endpoints';
import { Iabout } from './../model/Iabout';



@Injectable({
  providedIn: 'root'
})
export class AboutService {

  private aboutUrl: string = ENDPOINTS.about;

  
  constructor(private http: HttpClient) { 
    //empty
  }

  public getAboutData(): Observable<Iabout[]> {
    return this.http.get(this.aboutUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
