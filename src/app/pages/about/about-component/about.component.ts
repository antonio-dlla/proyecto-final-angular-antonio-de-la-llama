import { Component, OnInit } from '@angular/core';

import { Iabout } from './../model/Iabout';
import { AboutService } from './../service/about.service';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})

export class AboutComponent implements OnInit {

  public dataAbout!: Iabout[];

  constructor(private aboutService: AboutService) {
    //empty
   }

  ngOnInit(): void {
    this.getAboutData();
    console.log(this.dataAbout);
  }

  public getAboutData(): void {
    this.aboutService.getAboutData().subscribe(
      (data: Iabout[]) => {
        console.log(data);
        this.dataAbout = data;
      }
    )
  }

}
