import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSonComponent } from './form-son.component';

describe('FormSonComponent', () => {
  let component: FormSonComponent;
  let fixture: ComponentFixture<FormSonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormSonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormSonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
