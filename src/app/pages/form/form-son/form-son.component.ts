import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


import { Iform } from './../model/Iform';
import { FormService } from './../service/form.service';

@Component({
  selector: 'app-form-son',
  templateUrl: './form-son.component.html',
  styleUrls: ['./form-son.component.scss']
})
export class FormSonComponent implements OnInit {

  public dataForm!: Iform[];
  public signUpForm: FormGroup;
  public submitted: boolean = false;
  @Output() public newUserEmmit = new EventEmitter <Iform>();

  constructor(
    private formBuilder: FormBuilder,
    private formService: FormService,
  ) {
    this.signUpForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(1)]],
      surname: ['', [Validators.required, Validators.minLength(1)]],
      email: ['', [Validators.required, Validators.email]],
      message: ['', [Validators.required, Validators.maxLength(300)]],

    });
  }

  public onSubmit() {
    this.submitted = true;
    if (this.signUpForm.valid) {
      const newClient: Iform = {
        name: this.signUpForm?.get('name')?.value,
        surname: this.signUpForm?.get('surname')?.value,
        email: this.signUpForm?.get('email')?.value,
        message: this.signUpForm?.get('message')?.value,
        
      };
      this.formService.postUser(newClient).subscribe(
        (data) => {
          console.log(data)
        },
        (err) => {
          console.error(err.message);
        }
      )
      this.newUserOutput(newClient);
      this.signUpForm.reset();
      this.submitted = false;
      console.log(newClient);
    }
  }
  public newUserOutput(newClient: Iform) {
    this.newUserEmmit.emit(newClient);
    } 

  ngOnInit(): void {
    this.getFormData();
  }

  public getFormData(): void {
    this.formService.getFormData().subscribe(
      (data: Iform[]) => {
      console.log(data);
      this.dataForm = data;
    }
  );
  }
}
