import { Component, OnInit, Output, EventEmitter  } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


import { Iform } from './../model/Iform';
import { FormService } from './../service/form.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  public dataForm!: Iform[];

  constructor(private formService: FormService) {
    //empty
   }

  ngOnInit(): void {
    this.getFormData();
    console.log(this.dataForm);
  }

  public getFormData(): void {
    this.formService.getFormData().subscribe(
      (data: Iform[]) => {
        console.log(data);
        this.dataForm = data;
      }
    )
  }

}
