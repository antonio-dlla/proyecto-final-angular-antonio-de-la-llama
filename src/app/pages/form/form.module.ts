import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormRoutingModule } from './form-routing.module';
import { FormComponent } from './form-component/form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormSonComponent } from './form-son/form-son.component';


@NgModule({
  declarations: [FormComponent, FormSonComponent],
  imports: [
    CommonModule,
    FormRoutingModule,
    ReactiveFormsModule
  ]
})
export class FormModule { }
