import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';


import { ENDPOINTS } from '../../../../endpointUrls/endpoints';
import { Iform } from './../model/Iform';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  private formUrl: string = ENDPOINTS.form;
  private userUrl: string = ENDPOINTS.users;


  constructor(private http: HttpClient) {
    // empty
  }
  public getFormData(): Observable<Iform[]> {
    return this.http.get(this.formUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }

  public postUser(user: Iform): Observable<any> {
      return this.http.post(this.userUrl, user).pipe(
        map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
      
      );
  }

}
