import { Component, OnInit } from '@angular/core';

import { Ivideo } from './../model/Ivideo';
import { VideoService } from './../service/video.service';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.scss']
})
export class VideoComponent implements OnInit {

  public dataVideo!: Ivideo[];

  constructor(private videoService: VideoService) {
    //empty
   }

  ngOnInit(): void {
    this.getvideoData();
    console.log(this.dataVideo);
  }

  public getvideoData(): void {
    this.videoService.getVideoData().subscribe(
      (data: Ivideo[]) => {
        console.log(data);
        this.dataVideo = data;
      }
    )
  }

}
