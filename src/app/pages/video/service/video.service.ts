import { map, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';


import { ENDPOINTS } from '../../../../endpointUrls/endpoints';
import { Ivideo } from './../model/Ivideo';

@Injectable({
  providedIn: 'root'
})
export class VideoService {

  private videoUrl: string = ENDPOINTS.video;

  
  constructor(private http: HttpClient) { 
    //empty
  }

  public getVideoData(): Observable<Ivideo[]> {
    return this.http.get(this.videoUrl).pipe(
      map((response: any) => {
        if (!response) {
          throw new Error('Value expected');
        } else {
          return response;
        }
      }),
      catchError((err) => {
        throw new Error(err.message);
      })
    );
  }
}
