export const ENDPOINTS = {
    users: 'http://localhost:3000/users',
    about: 'http://localhost:3000/about',
    form: 'http://localhost:3000/form',
    foto: 'http://localhost:3000/foto',
    fotoDetail: 'http://localhost:3000/foto/',
    graphic: 'http://localhost:3000/graphic',
    home: 'http://localhost:3000/home',
    music: 'http://localhost:3000/music',
    video: 'http://localhost:3000/video',
    works: 'http://localhost:3000/works/'
};

